package com.ze.exercise.testmanager.repository;

import com.ze.exercise.testmanager.model.TestCaseResource;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.*;

//@Repository
//@Profile("map")
public class SimpleBackendByMapResourceRepository implements TestResourceRepository {
    private final Map<String, TestCaseResource> repo = new HashMap<>();

    @Override
    public Optional<TestCaseResource> findById(String id) {
        return Optional.ofNullable(repo.get(id));
    }

    @Override
    public List<TestCaseResource> findAll() {
        return new ArrayList<>(repo.values());
    }

    @Override
    public TestCaseResource save(TestCaseResource resource) {
        repo.put(resource.getId(), resource);
        return resource;
    }

    public void deleteAll() {
        repo.clear();
    }
}
