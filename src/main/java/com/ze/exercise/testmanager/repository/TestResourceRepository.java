package com.ze.exercise.testmanager.repository;

import com.ze.exercise.testmanager.model.TestCaseResource;

import java.util.List;
import java.util.Optional;

public interface TestResourceRepository {
    Optional<TestCaseResource> findById(String id);
    List<TestCaseResource> findAll();
    TestCaseResource save(TestCaseResource resource);
}
