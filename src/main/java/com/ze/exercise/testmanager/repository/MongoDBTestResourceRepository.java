package com.ze.exercise.testmanager.repository;

import com.ze.exercise.testmanager.model.TestCaseResource;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoDBTestResourceRepository extends TestResourceRepository, MongoRepository<TestCaseResource, String>  {

}
