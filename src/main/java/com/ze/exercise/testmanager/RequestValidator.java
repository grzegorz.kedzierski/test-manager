package com.ze.exercise.testmanager;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebInputException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

@Service
@RequiredArgsConstructor
public class RequestValidator {
    private static final int ID_MAX_LENGTH = 40;

    private final Validator validator;

    public <T> T validateAndReturnObj(T obj) {
        Set<ConstraintViolation<T>> errors = validator.validate(obj);
        if (!errors.isEmpty()) {
            String err = errors.stream()
                    .map(e -> format("Field '%s': %s", e.getPropertyPath(), e.getMessage()))
                    .collect(joining(", "));
            throw new ServerWebInputException(err);
        }
        // todo: add sanitization
        return obj;
    }

    public String validateIdAndReturnIt(String id) {
        if (StringUtils.isBlank(id)) {
            throw new ServerWebInputException("ID path parameter is blank.");
        } else if (id.length() >= ID_MAX_LENGTH) {
            throw new ServerWebInputException(format("ID is to long, should contains maximum %s characters", ID_MAX_LENGTH));
        }
        // todo: add better validation, sanitization
        return id;
    }
}
