package com.ze.exercise.testmanager.routes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.function.RouterFunction;
import org.springframework.web.servlet.function.RouterFunctions;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

import java.util.function.Function;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.servlet.function.RequestPredicates.accept;
import static org.springframework.web.servlet.function.RouterFunctions.route;

@Configuration
@Slf4j
public class TestCaseResourceRoutes {

    @Bean
    RouterFunction<ServerResponse> staticResourceRouter(){
        return RouterFunctions.resources("/**", new ClassPathResource("public/"));
    }

    @Bean
    RouterFunction<ServerResponse> routes(TestCaseResourceRoutesHandlers testCaseResourceRoutesHandlers) {
        return route()
                .path("/test-case", r -> r
                        .before(logRequest())
                        .POST("", accept(APPLICATION_JSON), testCaseResourceRoutesHandlers::post)
                        .PUT("/{id}", accept(APPLICATION_JSON), testCaseResourceRoutesHandlers::put)
                        .GET("/{id}", accept(APPLICATION_JSON), testCaseResourceRoutesHandlers::get)
                        .GET("", accept(APPLICATION_JSON), req -> testCaseResourceRoutesHandlers.getAll()))
                .build();
    }

    public Function<ServerRequest, ServerRequest> logRequest() {
        return req -> {
            log.info("Calling: {}", req.toString());
            return req;
        };
    }
}
