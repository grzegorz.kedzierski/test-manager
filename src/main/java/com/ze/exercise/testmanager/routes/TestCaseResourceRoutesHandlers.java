package com.ze.exercise.testmanager.routes;

import com.ze.exercise.testmanager.RequestValidator;
import com.ze.exercise.testmanager.model.TestCaseResource;
import com.ze.exercise.testmanager.model.TestCaseResourceInput;
import com.ze.exercise.testmanager.repository.TestResourceRepository;
import com.ze.exercise.testmanager.service.IdGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;

import static com.ze.exercise.testmanager.model.TestCaseResource.fromInput;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.servlet.function.ServerResponse.notFound;
import static org.springframework.web.servlet.function.ServerResponse.ok;

@Service
@RequiredArgsConstructor
@Slf4j
public class TestCaseResourceRoutesHandlers {
    private final RequestValidator validator;
    private final TestResourceRepository repository;
    private final IdGenerator idGenerator;

    public ServerResponse getAll() {
        List<TestCaseResource> allCases = repository.findAll();

        return ok().contentType(APPLICATION_JSON).body(allCases);
    }

    public ServerResponse get(ServerRequest request) {
        String id = validator.validateIdAndReturnIt(request.pathVariable("id"));

        return repository.findById(id)
                .map(res -> ok().contentType(APPLICATION_JSON).body(res))
                .orElse(notFound().build());
    }

    public ServerResponse put(ServerRequest request) throws ServletException, IOException {
        String id = validator.validateIdAndReturnIt(request.pathVariable("id"));
        TestCaseResourceInput input = parseBodyAndValidate(request);
        TestCaseResource testCaseResource = fromInput(id, input);

        repository.save(testCaseResource);

        return ok().contentType(APPLICATION_JSON).body(testCaseResource);
    }

    public ServerResponse post(ServerRequest request) throws ServletException, IOException {
        TestCaseResourceInput input = parseBodyAndValidate(request);
        TestCaseResource testCaseResource = fromInput(idGenerator.generate(), input);

        repository.save(testCaseResource);

        return ok().contentType(APPLICATION_JSON).body(testCaseResource);
    }

    private TestCaseResourceInput parseBodyAndValidate(ServerRequest request) throws ServletException, IOException {
        return validator.validateAndReturnObj(request.body(TestCaseResourceInput.class));
    }
}
