package com.ze.exercise.testmanager.service;

public interface IdGenerator {
    String generate();
}
