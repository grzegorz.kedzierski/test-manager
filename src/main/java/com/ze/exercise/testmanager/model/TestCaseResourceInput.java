package com.ze.exercise.testmanager.model;

import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
public class TestCaseResourceInput {
    @NotNull
    String name;
    @NotNull
    TestCaseStatus status;
}
