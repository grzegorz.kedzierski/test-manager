package com.ze.exercise.testmanager.model;

public enum TestCaseStatus {
    UNDEFINED, FAILED, PASSED
}
