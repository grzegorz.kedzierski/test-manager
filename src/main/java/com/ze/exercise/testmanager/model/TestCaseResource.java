package com.ze.exercise.testmanager.model;

import lombok.Value;

@Value
public class TestCaseResource {
    String id;
    String name;
    TestCaseStatus status;

    public static TestCaseResource fromInput(String id, TestCaseResourceInput testCaseResourceInput) {
        return new TestCaseResource(id, testCaseResourceInput.getName(), testCaseResourceInput.getStatus());
    }
}
