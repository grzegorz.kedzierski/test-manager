package com.ze.exercise.testmanager;

import org.junit.Test;
import org.springframework.web.server.ServerWebInputException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.stream.Stream;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RequestValidatorTest {

    @Test
    public void obj_validation_should_pass_when_validation_pass() {
        // given
        RequestValidator validator = new RequestValidator(validatorMockSuccess());
        // when
        String ret = validator.validateAndReturnObj("anything");
        // then
        assertThat(ret, equalTo("anything"));
    }

    @Test(expected = ServerWebInputException.class)
    public void obj_validation_should_fail_when_validation_fails() {
        // given
        RequestValidator validator = new RequestValidator(validatorMockFailure());
        // when, then should throw error
        validator.validateAndReturnObj("anything");
    }

    @Test
    public void id_validation_should_pass_for_valid_id() {
        // given
        RequestValidator validator = new RequestValidator(null);
        // when
        String ret = validator.validateIdAndReturnIt("123");
        // then
        assertThat(ret, equalTo("123"));
    }

    @Test(expected = ServerWebInputException.class)
    public void id_validation_should_fail_when_id_is_empty() {
        // given
        RequestValidator validator = new RequestValidator(null);
        // when, then should throw error
        validator.validateIdAndReturnIt("");
    }

    @Test(expected = ServerWebInputException.class)
    public void id_validation_should_fail_when_id_is_to_long() {
        // given
        RequestValidator validator = new RequestValidator(null);
        // when, then should throw error
        validator.validateIdAndReturnIt("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    }

    private Validator validatorMockSuccess() {
        Validator mock = mock(Validator.class);
        when(mock.validate(any())).thenReturn(emptySet());
        return mock;
    }

    private Validator validatorMockFailure() {
        Validator mock = mock(Validator.class);
        when(mock.validate(any())).thenReturn(Stream.of(mockConstraintViolation()).collect(toSet()));
        return mock;
    }

    @SuppressWarnings("unchecked")
    private ConstraintViolation<Object> mockConstraintViolation() {
        return mock(ConstraintViolation.class);
    }
}