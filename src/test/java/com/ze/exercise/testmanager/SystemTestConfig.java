package com.ze.exercise.testmanager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

import static com.ze.exercise.testmanager.TestManagerApplicationSystemTest.getMongoDBHost;
import static com.ze.exercise.testmanager.TestManagerApplicationSystemTest.getMongoDBPort;
import static java.lang.String.format;

@Profile("system-test")
@Configuration
@Slf4j
class SystemTestConfig {
    private static final String MONGO_USER = "test";
    private static final String MONGO_PASS = "test";
    private static final String MONGO_DB = "admin";

    @Bean
    public MongoDatabaseFactory mongoDbFactory() {
        String url = format("mongodb://%s:%s@%s:%s/%s", MONGO_USER, MONGO_PASS, getMongoDBHost(), getMongoDBPort(), MONGO_DB);
        log.info("--- Container mongo url: {}", url);
        return new SimpleMongoClientDatabaseFactory(url);
    }
}
