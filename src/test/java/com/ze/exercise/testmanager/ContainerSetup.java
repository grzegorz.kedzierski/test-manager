package com.ze.exercise.testmanager;

import lombok.extern.slf4j.Slf4j;
import org.junit.ClassRule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.File;

@Slf4j
public class ContainerSetup {

    private static final String MONGO_SERVICE_NAME = "mongodb";
    private static final int MONGO_SERVICE_PORT = 27017;

    @ClassRule
    private static final DockerComposeContainer<?> container =
            new DockerComposeContainer<>(new File("docker-compose-mongo.yml"))
                    .withExposedService(MONGO_SERVICE_NAME, MONGO_SERVICE_PORT);

    public static String getMongoDBHost() {
        return container.getServiceHost(MONGO_SERVICE_NAME, MONGO_SERVICE_PORT);
    }

    public static Integer getMongoDBPort() {
        return container.getServicePort(MONGO_SERVICE_NAME, MONGO_SERVICE_PORT);
    }

    @BeforeAll
    static void setUpAll() {
        container.start();
        log.info("--- Container MongoDB HOST: {}, PORT: {}", getMongoDBHost(), getMongoDBPort());
    }

    @AfterAll
    static void tearDownAll() {
        container.stop();
    }
}
