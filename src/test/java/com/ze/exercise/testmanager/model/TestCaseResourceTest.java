package com.ze.exercise.testmanager.model;

import org.junit.Test;

import static com.ze.exercise.testmanager.model.TestCaseStatus.PASSED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TestCaseResourceTest {

    @Test
    public void should_create_valid_case_from_input() {
        // given
        TestCaseResourceInput input = new TestCaseResourceInput("name", PASSED);
        // when
        TestCaseResource testCase = TestCaseResource.fromInput("id", input);
        // then
        assertThat(testCase, equalTo(new TestCaseResource("id", input.getName(), input.getStatus())));
    }
}