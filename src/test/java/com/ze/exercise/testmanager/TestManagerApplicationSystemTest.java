package com.ze.exercise.testmanager;

import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.junit.ClassRule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.ResourceUtils;
import org.testcontainers.containers.DockerComposeContainer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.Matchers.*;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"system-test", "mongo"})
@Slf4j
class TestManagerApplicationSystemTest extends ContainerSetup {

    @LocalServerPort
    private int port;

    @Test
    public void should_create_update_and_return_testcases() throws Exception {
        String jsonTemplate = getJsonTemplate();

        List<TestCaseResource> testCase = createTestCasesResources(
                new TestCaseResourceInput(jsonTemplate, "name1", "UNDEFINED"),
                new TestCaseResourceInput(jsonTemplate, "name2", "FAILED"),
                new TestCaseResourceInput(jsonTemplate, "name3", "PASSED")
        );

        TestCaseResource singleResource = testCase.get(0);

        getTestCaseResourceAndCheckResponse(singleResource);
        getTestCaseResourceNotFound("not-existing");

        updateTestCaseResourceAndCheckWithGet(jsonTemplate, singleResource);

        getAllTestCaseResourcesAndCheckResponse(testCase);
    }

    private List<TestCaseResource> createTestCasesResources(TestCaseResourceInput... resourceInputs) {
        return Stream.of(resourceInputs)
                .map(input -> {
                    String id = postTestCaseCheckResponseAndGetId(input);
                    return new TestCaseResource(id, input.getName(), input.getStatus());
                })
                .collect(toList());
    }

    private void updateTestCaseResourceAndCheckWithGet(String jsonTemplate, TestCaseResource singleResource) {
        putTestCaseCheckResponseAndGetId(new TestCaseResourceInput(jsonTemplate, "name3-changed", "FAILED"), singleResource.id);
        getTestCaseResourceAndCheckResponse(new TestCaseResource(singleResource.id, "name3-changed", "FAILED"));
    }


    private String getJsonTemplate() throws IOException {
        File file = ResourceUtils.getFile("classpath:testcase-request-template.json");
        return new String(Files.readAllBytes(file.toPath()));
    }

    private String postTestCaseCheckResponseAndGetId(TestCaseResourceInput input) {
        String json = input.toJsonString();
        // @formatter:off
        return
            given()
                .contentType(ContentType.JSON)
                .body(json)
            .when()
                .post(path("/test-case"))
            .then()
                .specification(validateTestCaseResponse(input.getName(), input.getStatus()))
                .extract().response().path("id");
        // @formatter:on
    }

    private void putTestCaseCheckResponseAndGetId(TestCaseResourceInput input, String id) {
        String json = input.toJsonString();
        // @formatter:off
        given()
            .pathParam("id", id)
            .contentType(ContentType.JSON)
            .body(json)
        .when()
            .put(path("/test-case/{id}"))
        .then()
            .specification(validateTestCaseResponse(input.getName(), input.getStatus()));
        // @formatter:on
    }

    private void getTestCaseResourceAndCheckResponse(TestCaseResource resource) {
        // @formatter:off
        given()
            .pathParam("id", resource.getId())
        .when()
            .get(path("/test-case/{id}"))
        .then()
            .specification(validateTestCaseResponse(resource.getName(), resource.getStatus()));
        // @formatter:on
    }

    private void getTestCaseResourceNotFound(String id) {
        // @formatter:off
        given()
            .pathParam("id", id)
        .when()
            .get(path("/test-case/{id}"))
        .then()
            .statusCode(404);
        // @formatter:on
    }

    private void getAllTestCaseResourcesAndCheckResponse(List<TestCaseResource> expected) {
        // @formatter:off
        Set<String> expectedIds = expected.stream().map(TestCaseResource::getId).collect(toSet());
        given()
                .when()
                .get(path("/test-case/"))
                .then()
                .specification(validateTestCaseResponse(expectedIds));
        // @formatter:on
    }

    private ResponseSpecification validateTestCaseResponse(Set<String> expectedIds) {
        // @formatter:off
        return
            expect()
                .statusCode(200)
                .body("size()", is(expectedIds.size()))
                .body("id", containsInAnyOrder(expectedIds.toArray()));
        // @formatter:on
    }

    private ResponseSpecification validateTestCaseResponse(String name, String status) {
        // @formatter:off
        return
            expect()
                .statusCode(200)
                .body("name", equalTo(name))
                .body("status", equalTo(status))
                .body("id", notNullValue());
        // @formatter:on
    }

    private String path(String s) {
        return "http://localhost:" + port + s;
    }

    @Value
    static class TestCaseResourceInput {
        String jsonTemplate;
        String name;
        String status;

        public String toJsonString() {
            return jsonTemplate
                    .replace("{name}", name)
                    .replace("{status}", status);
        }
    }

    @Value
    static class TestCaseResource {
        String id;
        String name;
        String status;
    }
}
