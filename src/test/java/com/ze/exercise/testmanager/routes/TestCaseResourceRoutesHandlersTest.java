package com.ze.exercise.testmanager.routes;

import com.ze.exercise.testmanager.RequestValidator;
import com.ze.exercise.testmanager.model.TestCaseResource;
import com.ze.exercise.testmanager.model.TestCaseResourceInput;
import com.ze.exercise.testmanager.repository.SimpleBackendByMapResourceRepository;
import com.ze.exercise.testmanager.service.IdGenerator;
import lombok.RequiredArgsConstructor;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.function.EntityResponse;
import org.springframework.web.servlet.function.ServerRequest;
import org.springframework.web.servlet.function.ServerResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Optional;

import static com.ze.exercise.testmanager.model.TestCaseStatus.UNDEFINED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestCaseResourceRoutesHandlersTest {
    private final ConstIdGenerator constIdGenerator = new ConstIdGenerator("1");
    private final SimpleBackendByMapResourceRepository repository = new SimpleBackendByMapResourceRepository();

    private final TestCaseResourceRoutesHandlers testCaseResourceRoutesHandlers =
            new TestCaseResourceRoutesHandlers(
                    mockRequestValidator(),
                    repository,
                    constIdGenerator);

    @Before
    public void beforeEach() {
        repository.deleteAll();
    }

    @Test
    public void get_should_return_cases() throws ServletException, IOException {
        // given
        TestCaseResource testCase = new TestCaseResource("123", "get_should_return_cases", UNDEFINED);
        repository.save(testCase);

        // when
        ServerResponse resp = testCaseResourceRoutesHandlers.get(mockServerRequest(testCase.getId()));

        // then
        assertThat(getEntity(resp), equalTo(testCase));
    }

    @Test
    public void get_all__should_return_all_cases() {
        // given
        TestCaseResource testCase1 = new TestCaseResource("123", "get_should_return_cases_1", UNDEFINED);
        TestCaseResource testCase2 = new TestCaseResource("456", "get_should_return_cases_2", UNDEFINED);
        repository.save(testCase1);
        repository.save(testCase2);

        // when
        ServerResponse resp = testCaseResourceRoutesHandlers.getAll();

        // then
        assertThat(getEntity(resp), containsInAnyOrder(testCase1, testCase2));
    }

    @Test
    public void post_should_save() throws ServletException, IOException {
        // given
        TestCaseResourceInput input = new TestCaseResourceInput("post_should_save", UNDEFINED);

        // when
        ServerResponse resp = testCaseResourceRoutesHandlers.post(mockServerRequest(input));

        // then
        TestCaseResource expected = TestCaseResource.fromInput(constIdGenerator.generate(), input);

        assertThat(repository.findById(constIdGenerator.generate()), equalTo(Optional.of(expected)));
        assertThat(getEntity(resp), equalTo(expected));
    }

    @Test
    public void put_should_save() throws ServletException, IOException {
        // given
        String id = "123";
        TestCaseResourceInput input = new TestCaseResourceInput("put_should_save", UNDEFINED);

        // when
        ServerResponse resp = testCaseResourceRoutesHandlers.put(mockServerRequest(input, id));

        // then
        TestCaseResource expected = TestCaseResource.fromInput(id, input);

        assertThat(repository.findById(id), equalTo(Optional.of(expected)));
        assertThat(getEntity(resp), equalTo(expected));
    }

    private RequestValidator mockRequestValidator() {
        RequestValidator mock = mock(RequestValidator.class);
        when(mock.validateAndReturnObj(any())).thenAnswer(invocation -> invocation.getArguments()[0]);
        when(mock.validateIdAndReturnIt(any())).thenAnswer(invocation -> invocation.getArguments()[0]);
        return mock;
    }

    private ServerRequest mockServerRequest(TestCaseResourceInput input) throws ServletException, IOException {
        return mockServerRequest(input, null);
    }

    private ServerRequest mockServerRequest(String id) throws ServletException, IOException {
        return mockServerRequest(null, id);
    }

    private ServerRequest mockServerRequest(TestCaseResourceInput input, String id) throws ServletException, IOException {
        ServerRequest mock = mock(ServerRequest.class);
        when(mock.body(TestCaseResourceInput.class)).thenReturn(input);
        when(mock.pathVariable("id")).thenReturn(id);
        return mock;
    }

    @SuppressWarnings("unchecked")
    private <T> T getEntity(ServerResponse resp) {
        return ((EntityResponse<T>) resp).entity();
    }

    @RequiredArgsConstructor
    static class ConstIdGenerator implements IdGenerator {
        private final String id;

        @Override
        public String generate() {
            return id;
        }
    }
}
