import { configureStore } from '@reduxjs/toolkit'

import reducer from './features/testcases/testCaseSlice'

export default configureStore({
  reducer: {
    testCases: reducer
  },
})
