import React from 'react'

import { TestCasesList } from './features/testcases/TestCasesList'
import { AddTestCase } from './features/testcases/AddTestCase'

function App() {
  return (
    <div className="App">
        <AddTestCase />
        <TestCasesList />
    </div>
  )
}

export default App
