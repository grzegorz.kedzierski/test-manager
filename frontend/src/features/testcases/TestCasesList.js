import React, {useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {selectAllCases, fetchTestCases, updateTestCasesStatus} from './testCaseSlice'
import {unwrapResult} from "@reduxjs/toolkit";

const TestCase = ({ testCase }) => {
  const dispatch = useDispatch()

  const onStatusChanged = async (event) => {
    try {
        let status = event.target.value
        let id = testCase.id
        let name = testCase.name
        const resultAction = await dispatch(
            updateTestCasesStatus({id, name, status})
        )
        unwrapResult(resultAction)
      } catch (err) {
        console.error('Failed to update the case status: ', err)
      }
    }

  return (
    <article className="cases-excerpt" key={testCase.id} id={testCase.id}>
      <div>Name: {testCase.name}</div>
      <div>Status:
        <select value={testCase.status} id="name" onChange={onStatusChanged}>
          <option value="UNDEFINED">Undefined</option>
          <option value="FAILED">Failed</option>
          <option value="PASSED">Passed</option>
        </select>
      </div>
    </article>
  )
}

export const TestCasesList = () => {
  const dispatch = useDispatch()
  const testCases = useSelector(selectAllCases)

  const status = useSelector((state) => state.testCases.status)
  const error = useSelector((state) => state.testCases.error)

  useEffect(() => {
    if (status === 'idle') {
      dispatch(fetchTestCases())
    }
  }, [status, dispatch])

  let content = () => {
      if (status === 'loading') {
          return <div className="loader">Loading...</div>
      } else if (status === 'succeeded') {
         return testCases.slice().reverse().map((testCase) => (
             <TestCase key={testCase.id} testCase={testCase}/>
         ))
      } else if (status === 'error') {
          return <div>{error}</div>
      }
  }

  return (
    <section className="cases-list">
      <h2>Test Cases</h2>
      {content()}
    </section>
  )
}
