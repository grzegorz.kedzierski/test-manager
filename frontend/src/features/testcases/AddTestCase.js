import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { unwrapResult } from '@reduxjs/toolkit'

import { createTestCases } from './testCaseSlice'

export const AddTestCase = () => {
  const [name, setName] = useState('')
  const [addRequestStatus, setAddRequestStatus] = useState('idle')
  const dispatch = useDispatch()

  const onNameChanged = (e) => setName(e.target.value)

  let isNotBlank = str => str && !/^\s*$/.test(str)
  const canSave = isNotBlank(name) && addRequestStatus === 'idle'

  const onSaveClicked = async () => {
    if (canSave) {
      try {
        setAddRequestStatus('pending')
        const resultAction = await dispatch(
          createTestCases(name)
        )
        unwrapResult(resultAction)
        setName('')
      } catch (err) {
        console.error('Failed to create the test event: ', err)
      } finally {
        setAddRequestStatus('idle')
      }
    }
  }

  return (
    <section>
      <h2>New Test Case</h2>
      <form>
        <label htmlFor="name">Name of the test case:</label>
        <input
          type="text"
          placeholder="Name of the case"
          value={name}
          onChange={onNameChanged}
        />
        <button type="button" onClick={onSaveClicked} disabled={!canSave}>
          Create
        </button>
      </form>
    </section>
  )
}
