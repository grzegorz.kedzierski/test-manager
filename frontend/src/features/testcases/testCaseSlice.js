import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { client } from '../../api/client'

const initialState = {
  testCase: [],
  status: 'idle',
  error: null,
}

export const fetchTestCases = createAsyncThunk(
    'testCase/fetchTestCases',
    async () => await client.get('/test-case')
)

export const createTestCases = createAsyncThunk(
  'testCase/createTestCases',
  async (name) => await client.post('/test-case', {
    name,
    status: "UNDEFINED"
  })
)

export const updateTestCasesStatus = createAsyncThunk(
    'testCase/updateTestCasesStatus',
    async ({id, name, status}) => await client.put(`/test-case/${id}`, {
      name,
      status
    })
)

const testCaseSlice = createSlice({
  name: 'testCases',
  initialState,
  reducers: {},
  extraReducers: {
    [fetchTestCases.pending]: (state, action) => {
      state.status = 'loading'
    },
    [fetchTestCases.fulfilled]: (state, action) => {
      state.status = 'succeeded'
      state.testCase = state.testCase.concat(action.payload)
    },
    [fetchTestCases.rejected]: (state, action) => {
      state.status = 'failed'
      state.error = action.payload
    },
    [createTestCases.fulfilled]: (state, action) => {
      state.testCase.push(action.payload)
    },
    [updateTestCasesStatus.fulfilled]: (state, action) => {
      state.testCase.find((testCase) => testCase.id === action.payload.id).status = action.payload.status
    }
  },
})

export default testCaseSlice.reducer

export const selectAllCases = (state) => state.testCases.testCase
