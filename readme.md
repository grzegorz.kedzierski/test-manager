# Simple Test Manager App

## Big TODOs
- add frontend tests
- add e2e tests

## How to build fat-jar with both frontend and backend

1. Inside `frontend/` directory run:
    ```
    npm install
    npm run build
    ```
2. In root directory run:
    ```
    ./gradlew bootJar
    ```

### How to run develop application
1. **Prerequisite**:
    - use `docker-compose` to run mongodb
        ```
        docker-compose -f docker-compose-mongo.yml up 
        ```
2. **If you want to develop frontend app and run it**: 
   - run backend application
     ```
     ./gradlew bootRun   
     ```
   - then go into frontend folder and run react application
     ```
     cd frontend/
     npm start
     ```
3. **If you want to develop only backend app**: 
     - Start the backend application either with gradle 
        ```
        ./gradlew bootRun
        ```
     - or directly by 
      running main method from `com.ze.exercise.testmanager.TestManagerApplication`
       